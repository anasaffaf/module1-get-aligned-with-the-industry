##Product Name:  
Deep Armor  
##Product Link:  
[Link](https://www.sparkcognition.com/products/deeparmor/)  
##Product Short Description:  
DeepArmor is an AI-built cybersecurity solution that improves your security posture with industry-leading zero-day protection against today’s most advanced ransomware, viruses, malware, and more.
##Product is combination of features:
1. Object Detection
2. ID Recognition
3. Person Detection

##Product is Provided by which company
Sparkcognition
_____________________________________________________________________________________________________________________________________________________

##Product Name:  
DeepNLP  
##Product Link:  
[Link](https://www.sparkcognition.com/products/deepnlp/)  
##Product Short Description:  
Upload Unstructured Data. Streamline Business Decisions.  
##Product is combination of features:
1. NLP
2. ID Recognition

##Product is Provided by which company  
Sparkcognition